'use strict';

var request = require('supertest');
var app = require("../server").getApp;

var url = 'http://localhost:3000';

var profile = {
  uid: 'abag',
  pwd: '3a7ncOUA6ixT'
};


describe('Group Controller', function() {
  before(function() {
    // runs before all tests in this block
  });

  it('POST /groups should return a list of groups', function(done) {
    request(app)
    .post('/groups')
    .set('Content-Type', 'application/x-www-form-urlencoded')
    .send(profile)
    .expect(200, done);
  });
});

describe('Series Controller', function() {
  before(function() {
    // runs before all tests in this block
  });

  it('POST /catalog should return a list of series', function(done) {
    request(app)
    .post('/catalog')
    .set('Content-Type', 'application/x-www-form-urlencoded')
    .send(profile)
    .expect(200, done);
  });

  it('POST /catalog/:view should return a single series', function(done) {
    request(app)
    .post('/catalog/pib')
    .set('Content-Type', 'application/x-www-form-urlencoded')
    .send(profile)
    .expect(200, done);
  });

  it('POST /series/:view should return data of that series', function(done) {
    request(app)
    .post('/series/pib')
    .set('Content-Type', 'application/x-www-form-urlencoded')
    .send(profile)
    .expect(200, done);
  });

  it('POST /agenda should return a list of events', function(done) {
    request(app)
    .post('/agenda')
    .set('Content-Type', 'application/x-www-form-urlencoded')
    .send(profile)
    .expect(200, done);
  });


});