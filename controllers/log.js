// Load required packages
var Log = require('../models/log');

// Create endpoint /api/users for POST
exports.logAction = function(req, res, callback) {
  var log = new Log({
    username: req.user.username,
    endpoint: req.url,
    timestamp: new Date(),
    IP: req.connection.remoteAddress
  });

  log.save(function(err) {
    return callback();
  });
};