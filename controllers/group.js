'use strict';

var mysqlModel = require('mysql-model');

var AppModel = mysqlModel.createConnection({
  host     : process.env.AWS_MYSQL_HOST,
  port     : process.env.AWS_MYSQL_PORT,
  user     : process.env.AWS_MYSQL_USER,
  password : process.env.AWS_MYSQL_PASSWORD,
  database : 'tables'
});

var group = new AppModel({tableName: '`abag`.`group`'});

// create a model using the name of the DynamoDB table and a schema


exports.getAll = function(req, res) {
  group.find('all', function(err, rows) {
    if (err) {
      throw err;
    }
    res.send(rows);
  });
};