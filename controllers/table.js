'use strict';

var mysqlModel = require('mysql-model');

var AppModel = mysqlModel.createConnection({
  host     : process.env.AWS_MYSQL_HOST,
  port     : process.env.AWS_MYSQL_PORT,
  user     : process.env.AWS_MYSQL_USER,
  password : process.env.AWS_MYSQL_PASSWORD,
  database : 'tables'
});


exports.getTablesByView = function(req, res) {

	var table = new AppModel({tableName: '`' + req.user.username + '`.`table`'});
	table.query('SELECT c.*, `column`, `value` FROM `' 
	+ req.user.username + '`.table c LEFT JOIN `' 
	+ req.user.username + '`.table_params cp ON cp.`table_id` = c.`id` WHERE view = \'' 
	+ req.params.view + '\'', function(err, rows) {
	    if (err) {
	      console.error(err);
	      throw err;
	    }
		var result = {};
		var item, config, c;
		for (var i in rows) {
			item = rows[i];
			config = {};
			if (result[item.id]) {
				config = result[item.id].config || config;
			}
			if (item.column) {
				if (!config[item.column]) {
				  config[item.column] = [];
				}
				config[item.column].push(item.value);
			}
			result[item.id] = {table: item, config: config};
	    }

	    var toList = [];
	    for (var r in result) {
			toList.push(result[r]);
	    }

	    res.send(toList);
	});
};