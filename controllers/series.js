'use strict';

var mysqlModel = require('mysql-model');

var AppModel = mysqlModel.createConnection({
  host     : process.env.AWS_MYSQL_HOST,
  port     : process.env.AWS_MYSQL_PORT,
  user     : process.env.AWS_MYSQL_USER,
  password : process.env.AWS_MYSQL_PASSWORD
});

exports.getAllCatalog = function(req, res) {

  var catalog = new AppModel({tableName: '`' + req.user.username + '`.`catalog`'});

  // catalog.find('all', {limit: [0, req.body.limit || 1000]}, function(err, rows) {
  //   if (err) {
  //     console.log(err);
  //     throw err;
  //   }
  //   res.send(rows);
  // });
  catalog.query('SELECT `catalog`.id, view, nickname, name as group_name, `group`.id as group_id, source, observation FROM `' + req.user.username + 
    '`.`catalog` INNER JOIN `' + req.user.username + 
    '`.`group` ON `catalog`.`group` = `group`.`id`', function(err, rows) {
    if (err) {
      console.log(err);
      throw err;
    }
    res.send(rows);
  });
};

exports.getCatalogByView = function(req, res) {

  var catalog = new AppModel({tableName: '`' + req.user.username + '`.`catalog`'});

  catalog.find('first', {where: 'view = \'' + req.params.view + '\''}, function(err, rows) {
    if (err) {
      console.log(err);
      throw err;
    }
    res.send(rows);
  });
};

exports.getView = function(req, res) {

  //user has its own database
  var table = new AppModel({tableName: '`' + req.user.username + '`.`' + req.params.view + '`'});

  table.find('all', {limit: [0, req.body.limit || 10000]}, function(err, rows) {
    if (err) {
      console.log(err);
      throw err;
    }
    res.send(rows);
  });
};
