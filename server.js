// Load required packages
var express = require('express');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var ejs = require('ejs');
var session = require('express-session');
var passport = require('passport');
var seriesController = require('./controllers/series');
var agendaController = require('./controllers/agenda');
var groupController = require('./controllers/group');
var userController = require('./controllers/user');
var authController = require('./controllers/auth');
var oauth2Controller = require('./controllers/oauth2');
var clientController = require('./controllers/client');
var logController = require('./controllers/log');
var chartController = require('./controllers/chart');
var tableController = require('./controllers/table');


var cors = {
  origin: ['null', 'localhost:3000'],
  default: 'http://abag.parallaxis.com.br'
};

//CORS middleware
var allowCrossDomain = function(req, res, next) {
    //res.header('Access-Control-Allow-Origin', 'example.com');
    //testing purposes
    //var origin = cors.origin.indexOf(req.header('host').toLowerCase()) > -1 ? req.headers.origin : cors.default;
    //res.header('Access-Control-Allow-Origin', origin);
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    //res.header('Access-Control-Allow-Origin', 'http://abag.parallaxis.com.br');
    res.header('Access-Control-Allow-Methods', 'POST');
    //res.header('Access-Control-Allow-Headers', 'Content-Type');

    next();
}

// Connect to the beerlocker MongoDB
mongoose.connect('mongodb://localhost:27017/webservice');

// Create our Express application
var app = express();

// Set view engine to ejs
app.set('view engine', 'ejs');

// Use the body-parser package in our application
app.use(bodyParser.urlencoded({
  extended: true
}));

// Use express session support since OAuth2orize requires it
app.use(session({ 
  secret: process.env.SESSION_SECRET || 'Super Secret Session Key',
  saveUninitialized: true,
  resave: true
}));

// Use the passport package in our application
app.use(passport.initialize());

app.use(allowCrossDomain);

// Create our Express router
var router = express.Router();

router.route('/catalog')
  .post(authController.isAuthenticated, logController.logAction, seriesController.getAllCatalog);

router.route('/catalog/:view')
  .post(authController.isAuthenticated, logController.logAction, seriesController.getCatalogByView);

router.route('/groups')
  .post(authController.isAuthenticated, logController.logAction, groupController.getAll);

router.route('/series/:view')
  .post(authController.isAuthenticated, logController.logAction, seriesController.getView);

router.route('/agenda')
  .post(authController.isAuthenticated, logController.logAction, agendaController.getAll);

router.route('/charts/:view')
  .post(authController.isAuthenticated, logController.logAction, chartController.getChartsByView);

router.route('/tables/:view')
  .post(authController.isAuthenticated, logController.logAction, tableController.getTablesByView);  

// Create endpoint handlers for /users
//router.route('/api/users')
//  .post(userController.postUsers) //TODO: remove this possibility when in production
//  .get(authController.isAuthenticated, userController.getUsers);

// Register all our routes
app.use(router);

// Start the server
app.listen(3000);

module.exports.getApp = app;