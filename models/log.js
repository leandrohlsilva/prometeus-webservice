// Load required packages
var mongoose = require('mongoose');

// Define our user schema
var LogSchema = new mongoose.Schema({
  username: {
    type: String,
    required: true
  },
  endpoint: {
    type: String,
    required: true
  },
  timestamp: {
    type: Date,
    index: true,
    required: true
  },
  IP: String
});

// Export the Mongoose model
module.exports = mongoose.model('Log', LogSchema);